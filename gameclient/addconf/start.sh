#!/bin/bash
service php5.6-fpm start
memcached -u www-data &
nginx -g 'daemon off;'