#!/bin/bash
KEY_NAME=nginx
SETPATH=/etc/nginx/ssl
SETOU=MyUNIT
SETCN=MYHOST


/usr/bin/openssl genrsa -out ${SETPATH}/${KEY_NAME}.key 1024 && \

/usr/bin/openssl req  -new -key ${SETPATH}/${KEY_NAME}.key -subj "/C=TW/ST=TAIWAN/L=Taipai/O=company/OU=${SETOU}/CN=${SETCN}" -out ${SETPATH}/${KEY_NAME}.csr  && \

/usr/bin/openssl x509 -req -days 10000 -signkey ${SETPATH}/${KEY_NAME}.key -in ${SETPATH}/${KEY_NAME}.csr -out ${SETPATH}/${KEY_NAME}.crt